<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Web_App_Model extends CI_Model {

	/**
	 * @author **/

	  function search($keyword)
    {
        $this->db->like('nip',$keyword);
        $query  =   $this->db->get('pegawai');
        return $query->result();
    }
	 
	//query otomatis dengan active record
	public function getAllData($table)
	{
		return $this->db->get($table);
	}
	
	public function getAllDataLimited($table,$offset,$limit)
	{
		return $this->db->get($table,$limit,$offset);
	}
	
	public function getSelectedData($table,$key,$value)
	{
		$this->db->where($key, $value); 
		return $this->db->get($table);
	}
	
	public function getSelectedDataMultiple($table,$data)
	{
		return $this->db->get_where($table, $data);
	}
	
	function deleteData($table,$data)
	{
		$this->db->delete($table, $data);
	}
	
	function updateData($table,$data,$field,$key)
	{
		$this->db->where($key,$field);
		$this->db->update($table,$data);
	}
	
	function updateDataMultiField($table,$data,$field_key)
	{
		$this->db->update($table,$data,$field_key);
	}
	
	function insertData($table,$data)
	{
		$this->db->insert($table,$data);
	}
	
	
	//query login
	public function getLoginData($usr,$psw)
	{
		$u = mysql_real_escape_string($usr);
		$p = md5(mysql_real_escape_string($psw));
		$q_cek_login = $this->db->get_where('tbl_login', array('username' => $u, 'password' => $p));
		if(count($q_cek_login->result())>0)
		{
			foreach($q_cek_login->result() as $qck)
			{
				if($qck->stts=='mahasiswa')
				{
					$q_ambil_data = $this->db->get_where('tbl_mahasiswa', array('nim' => $u));
					foreach($q_ambil_data ->result() as $qad)
					{
						$sess_data['logged_in'] = 'yes';
						$sess_data['nim'] = $qad->nim;
						$sess_data['nama'] = $qad->nama_mahasiswa;
						$sess_data['angkatan'] = $qad->angkatan;
						$sess_data['jurusan'] = $qad->jurusan;
						$sess_data['stts'] = 'mahasiswa';
						$sess_data['program'] = $qad->kelas_program;
						$this->session->set_userdata($sess_data);
					}
					header('location:'.base_url().'mahasiswa');
				}
			}
		}
		else
		{
			header('location:'.base_url().'web');
		}
	}

	function getEditSertifikat($id_acak) {
		return $this->db->query("SELECT a.id_acak,  a.status FROM penduduk a where a.id_acak='".$id_acak."'");
	}

// 	function list_data($pencarian,$offset,$total){
//        if ($pencarian){
//   $this->db->like('nim',$pencarian);
//  }     
//  $result['total_rows'] = $this->db->count_all_results('tb_sertifikat');
  
//  if ($pencarian){
//   $this->db->like('nim',$pencarian);
//  }
//  $query = $this->db->get('tb_sertifikat',$total,$offset);
        
//  $result['data'] = $query->result(); 
//  return $result;
//    }
}

/* End of file web_app_model.php */
/* Location: ./application/models/web_app_model.php */