-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 30 Jul 2019 pada 18.17
-- Versi Server: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `e_absensi`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `absensi`
--

CREATE TABLE `absensi` (
  `id_absensi` int(11) NOT NULL,
  `id_pegawai` int(11) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `mulai_masuk` time DEFAULT NULL,
  `jam_telat` time DEFAULT NULL,
  `telat` int(11) DEFAULT NULL,
  `jam_masuk` datetime DEFAULT NULL,
  `jam_pulang` datetime DEFAULT NULL,
  `masuk` int(11) DEFAULT NULL,
  `keterangan` varchar(50) DEFAULT NULL,
  `latitude` text NOT NULL,
  `longtitude` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `absensi`
--

INSERT INTO `absensi` (`id_absensi`, `id_pegawai`, `tanggal`, `mulai_masuk`, `jam_telat`, `telat`, `jam_masuk`, `jam_pulang`, `masuk`, `keterangan`, `latitude`, `longtitude`) VALUES
(102, 8, '2019-07-01', '19:30:00', '00:37:32', 1, '2019-07-01 18:52:28', NULL, 1, 'Masuk Telat 00:37:32', '', ''),
(103, 12, '2019-07-01', '19:30:00', '00:36:42', 1, '2019-07-01 18:53:18', NULL, 1, 'Masuk Telat 00:36:42', '', ''),
(114, 8, '2019-07-03', '17:30:00', '00:00:00', 0, '2019-07-03 17:18:21', NULL, 1, 'Masuk', '', ''),
(115, 10, '2019-07-03', '17:30:00', '00:00:00', 0, '2019-07-03 17:18:28', NULL, 1, 'Masuk', '', ''),
(124, 12, '2019-07-04', '17:30:00', '02:18:10', 1, '2019-07-04 19:48:10', NULL, 1, 'Masuk Telat 02:18:10', '', ''),
(125, 8, '2019-07-04', '17:30:00', '02:48:42', 1, '2019-07-04 20:18:42', NULL, 1, 'Masuk Telat 02:48:42', '', ''),
(127, 8, '2019-07-07', '12:30:00', '01:02:17', 1, '2019-07-07 13:32:17', NULL, 1, 'Masuk Telat 01:02:17', '', ''),
(132, 10, '2019-07-22', '17:10:00', '00:38:52', 1, '2019-07-22 17:48:52', NULL, 1, 'Masuk Telat 00:38:52', '', ''),
(133, 12, '2019-07-20', '17:10:00', '00:41:57', 1, '2019-07-20 17:51:57', NULL, 1, 'Masuk Telat 00:41:57', '', ''),
(134, 6, '2019-07-21', '02:30:00', '00:00:00', 0, '2019-07-21 02:15:14', NULL, 1, 'Masuk', '', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `coba`
--

CREATE TABLE `coba` (
  `user_id` int(2) NOT NULL,
  `nama` varchar(10) NOT NULL,
  `alamat` text NOT NULL,
  `kontak` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `cuti`
--

CREATE TABLE `cuti` (
  `id_cuti` varchar(50) NOT NULL,
  `id_pegawai` int(11) DEFAULT NULL,
  `tanggal_pengajuan` date DEFAULT NULL,
  `tanggal_mulai` date DEFAULT NULL,
  `tanggal_selesai` date DEFAULT NULL,
  `jumlah_hari` int(11) DEFAULT NULL,
  `jenis_cuti` varchar(50) DEFAULT NULL,
  `keterangan` text,
  `approve` int(11) DEFAULT '0',
  `approve_by` varchar(50) DEFAULT NULL,
  `file` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `cuti`
--

INSERT INTO `cuti` (`id_cuti`, `id_pegawai`, `tanggal_pengajuan`, `tanggal_mulai`, `tanggal_selesai`, `jumlah_hari`, `jenis_cuti`, `keterangan`, `approve`, `approve_by`, `file`) VALUES
('0001/PML/CUTI/06/2019', 8, '2019-06-30', '2019-06-30', '2019-07-01', 1, 'sakit', 'sakit', 2, 'admin', NULL),
('0002/PML/CUTI/06/2019', 11, '2019-06-30', '2019-06-30', '2019-07-01', 1, 'sakit', 'sakit', 0, 'admin', NULL),
('0003/PML/CUTI/06/2019', 10, '2019-06-30', '2019-06-30', '2019-07-05', 1, 'sakit', 'sakit', 1, 'admin', NULL),
('0004/PML/CUTI/07/2019', 10, '2019-07-20', '2019-07-20', '2019-07-21', 0, 'sakit', 'sakit', 2, 'admin', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `detailcuti`
--

CREATE TABLE `detailcuti` (
  `id_pegawai` int(11) DEFAULT NULL,
  `id_cuti` int(11) DEFAULT NULL,
  `tanggal_pengajuan` date DEFAULT NULL,
  `tanggal_cuti` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `detailcuti`
--

INSERT INTO `detailcuti` (`id_pegawai`, `id_cuti`, `tanggal_pengajuan`, `tanggal_cuti`) VALUES
(11, 2, '2019-06-30', '2019-06-30'),
(11, 2, '2019-06-30', '2019-07-01'),
(13, 4, '2019-07-05', '2019-07-07'),
(13, 4, '2019-07-05', '2019-07-08'),
(10, 4, '2019-07-20', '2019-07-20'),
(10, 4, '2019-07-20', '2019-07-21');

-- --------------------------------------------------------

--
-- Struktur dari tabel `detaildinas`
--

CREATE TABLE `detaildinas` (
  `id_dinas` varchar(50) DEFAULT NULL,
  `id_pegawai` int(11) DEFAULT NULL,
  `tanggal_pengajuan` date DEFAULT NULL,
  `tanggal_dinas` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data untuk tabel `detaildinas`
--

INSERT INTO `detaildinas` (`id_dinas`, `id_pegawai`, `tanggal_pengajuan`, `tanggal_dinas`) VALUES
('DNL-0001', 12, '2019-07-27', '2019-07-28'),
('DNL-0001', 12, '2019-07-27', '2019-07-29'),
('DNL-0001', 12, '2019-07-27', '2019-07-30');

-- --------------------------------------------------------

--
-- Struktur dari tabel `dinas`
--

CREATE TABLE `dinas` (
  `id_dinas` varchar(50) NOT NULL,
  `id_pegawai` int(11) DEFAULT NULL,
  `tanggal_pengajuan` date DEFAULT NULL,
  `tanggal_mulai` date DEFAULT NULL,
  `tanggal_selesai` date DEFAULT NULL,
  `jumlah_hari` int(11) DEFAULT NULL,
  `keterangan` text,
  `file` text,
  `tempat` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data untuk tabel `dinas`
--

INSERT INTO `dinas` (`id_dinas`, `id_pegawai`, `tanggal_pengajuan`, `tanggal_mulai`, `tanggal_selesai`, `jumlah_hari`, `keterangan`, `file`, `tempat`) VALUES
('DNL-0001', 12, '2019-07-27', '2019-07-28', '2019-07-30', 2, 'melakukan survei', NULL, 'Kab. Garut');

-- --------------------------------------------------------

--
-- Struktur dari tabel `divisi`
--

CREATE TABLE `divisi` (
  `id_divisi` int(11) NOT NULL,
  `nama_divisi` varchar(50) DEFAULT NULL,
  `keterangan` varchar(50) DEFAULT NULL,
  `create_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `divisi`
--

INSERT INTO `divisi` (`id_divisi`, `nama_divisi`, `keterangan`, `create_date`, `update_date`) VALUES
(1, 'Kepala BPS Kota Bandung', 'lantai 2', '2019-02-18 03:47:00', NULL),
(13, 'Kepala Seksi Statistik Sosial', 'lantai 2', '2019-03-25 13:47:04', NULL),
(14, 'Kepala Seksi Statistik Distribusi', 'lantai 2', '2019-04-11 13:51:09', NULL),
(15, 'Kepala Seksi Statistik Produksi', 'lantai 2', '2019-04-11 13:51:49', NULL),
(16, 'Kepala Seksi Statistik Neraca', 'lantai 2', '2019-04-11 13:52:41', NULL),
(17, 'Kepala Seksi Statistik IPDS', 'lantai 1', '2019-04-11 13:53:16', NULL),
(18, 'Kepala Seksi Statistik Tata Usaha', 'lantai 1', '2019-04-11 13:53:53', NULL),
(20, 'Koordinator Statistik Kecamatan (KSK)', 'lantai 1', '2019-04-15 02:11:04', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `harilibur`
--

CREATE TABLE `harilibur` (
  `id` int(11) NOT NULL,
  `tanggal_libur` date DEFAULT NULL,
  `keterangan` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `harilibur`
--

INSERT INTO `harilibur` (`id`, `tanggal_libur`, `keterangan`) VALUES
(1, '2019-04-03', 'Isra Miraj'),
(2, '2019-04-19', 'Wafat isa almasih'),
(4, '2019-04-21', 'hari paskah'),
(8, '2019-05-01', 'Hari buruh'),
(9, '2019-05-19', 'Hari raya waisak'),
(10, '2019-04-17', 'Pemilu'),
(11, '2019-06-27', 'ada');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jabatan`
--

CREATE TABLE `jabatan` (
  `id_jabatan` int(11) NOT NULL,
  `nama_jabatan` varchar(50) DEFAULT NULL,
  `keterangan` varchar(50) DEFAULT NULL,
  `create_date` timestamp NULL DEFAULT NULL,
  `update_date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data untuk tabel `jabatan`
--

INSERT INTO `jabatan` (`id_jabatan`, `nama_jabatan`, `keterangan`, `create_date`, `update_date`) VALUES
(1, 'Ketua', 'Badan Pusat Statistik Kota Bandung', '2019-02-18 03:47:30', NULL),
(2, 'Kasie Sosial', 'Badan Pusat Statistik Kota Bandung', NULL, NULL),
(3, 'Anggota Sosial', 'Badan Pusat Statistik Kota Bandung', NULL, NULL),
(4, 'Kasie Distribusi', 'Badan Pusat Statistik Kota Bandung', NULL, NULL),
(5, 'Anggotra Distribusi', 'Badan Pusat Statistik Kota Bandung', NULL, NULL),
(6, 'Kasie Produksi', 'Badan Pusat Statistik Kota Bandung', NULL, NULL),
(7, 'Anggota Produksi', 'Badan Pusat Statistik Kota Bandung', NULL, NULL),
(8, 'Kasie Neraca', 'Badan Pusat Statistik Kota Bandung', NULL, NULL),
(9, 'Anggota Neraca', 'Badan Pusat Statistik Kota Bandung', NULL, NULL),
(10, 'Kasie IPDS', 'Badan Pusat Statistik Kota Bandung', NULL, NULL),
(11, 'Anggota IPDS', 'Badan Pusat Statistik Kota Bandung', NULL, NULL),
(12, 'Kasie TU', 'Badan Pusat Statistik Kota Bandung', NULL, NULL),
(13, 'Anggota TU', 'Badan Pusat Statistik Kota Bandung', NULL, NULL),
(16, 'KSK', 'Badan Pusat Statistik Kota Bandung', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `level`
--

CREATE TABLE `level` (
  `id_level` int(11) NOT NULL,
  `nama_level` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `level`
--

INSERT INTO `level` (`id_level`, `nama_level`) VALUES
(1, 'user'),
(2, 'admin'),
(3, 'administrator');

-- --------------------------------------------------------

--
-- Struktur dari tabel `log_activity`
--

CREATE TABLE `log_activity` (
  `id_log` int(11) NOT NULL,
  `nip` varchar(29) DEFAULT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `time` datetime DEFAULT NULL,
  `keterangan` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `log_activity`
--

INSERT INTO `log_activity` (`id_log`, `nip`, `nama`, `tanggal`, `time`, `keterangan`) VALUES
(33, '111111111111111000', 'Likhit Ari Prabowo', '2019-06-26', '2019-06-26 20:55:25', 'Masuk Telat 12:55:25'),
(34, '111111111111111000', 'Likhit Ari Prabowo', '2019-06-26', '2019-06-26 20:55:40', 'pulang'),
(35, '111111111111111000', 'Likhit Ari Prabowo', '2019-06-26', '2019-06-26 20:56:29', 'pulang'),
(36, '196108141990032001', 'Dra. Lilis Pujiawati', '2019-06-27', '2019-06-27 08:56:16', 'Masuk Telat 00:56:16'),
(37, '196711261990032001', 'Ely Sugiharti, S.Si., M.AP', '2019-06-27', '2019-06-27 08:59:16', 'Masuk Telat 01:29:16'),
(38, '196711261990032001', 'Ely Sugiharti, S.Si., M.AP', '2019-06-27', '2019-06-27 08:59:54', 'pulang'),
(39, '198512182011011010', 'Ade Setyadi, S.I.Kom', '2019-06-27', '2019-06-27 10:41:19', 'Masuk Telat 03:11:19'),
(40, '000000000000000000', 'admin', '2019-06-27', '2019-06-27 10:43:03', 'Masuk Telat 03:13:03'),
(41, '000000000000000000', 'admin', '2019-06-27', '2019-06-27 10:51:41', 'Masuk Telat 03:21:41'),
(42, '198512182011011010', 'Ade Setyadi, S.I.Kom', '2019-06-27', '2019-06-27 11:07:30', 'Masuk Telat 03:37:30'),
(43, '196108141990032001', 'Dra. Lilis Pujiawati', '2019-06-29', '2019-06-29 14:22:22', 'Masuk Telat 06:52:22'),
(44, '196108141990032001', 'Dra. Lilis Pujiawati', '2019-06-30', '2019-06-30 14:24:31', 'Masuk Telat 06:54:31'),
(45, '196108141990032001', 'Dra. Lilis Pujiawati', '2019-06-21', '2019-06-21 14:54:24', 'Masuk Telat 16:35:36'),
(46, '196108141990032001', 'Dra. Lilis Pujiawati', '2019-06-27', '2019-06-27 20:54:06', 'Masuk'),
(47, '196108141990032001', 'Dra. Lilis Pujiawati', '2019-06-27', '2019-06-27 20:54:16', 'pulang'),
(48, '000000000000000000', 'admin', '2019-06-27', '2019-06-27 22:52:02', 'Masuk'),
(49, '000000000000000000', 'admin', '2019-06-27', '2019-06-27 22:54:05', 'Masuk'),
(50, '000000000000000000', 'admin', '2019-06-27', '2019-06-27 22:55:10', 'Masuk'),
(51, '000000000000000000', 'admin', '2019-06-27', '2019-06-27 22:56:00', 'Masuk'),
(52, '000000000000000000', 'admin', '2019-06-27', '2019-06-27 22:59:09', 'Masuk'),
(53, '000000000000000000', 'admin', '2019-06-27', '2019-06-27 23:01:21', 'Masuk'),
(54, '000000000000000000', 'admin', '2019-06-27', '2019-06-27 23:05:39', 'Masuk Telat 15:35:39'),
(55, '198512182011011010', 'Ade Setyadi, S.I.Kom', '2019-06-28', '2019-06-28 08:43:30', 'Masuk Telat 00:03:30'),
(56, '196711261990032001', 'Ely Sugiharti, S.Si., M.AP', '2019-06-30', '2019-06-30 05:06:31', 'Masuk'),
(57, '196108141990032001', 'Dra. Lilis Pujiawati', '2019-06-30', '2019-06-30 05:07:26', 'Masuk'),
(58, '196108141990032002', 'Ade Setyadi, S.I.Kom', '2019-06-30', '2019-06-30 13:38:41', 'Masuk Telat 06:08:41'),
(59, '196108141990032001', 'Dra. Lilis Pujiawati', '2019-06-30', '2019-06-30 13:38:55', 'Masuk Telat 06:08:55'),
(60, '196711261990032001', 'Ely Sugiharti, S.Si., M.AP', '2019-06-30', '2019-06-30 13:39:06', 'Masuk Telat 06:09:06'),
(61, '000000000000000000', 'admin', '2019-06-30', '2019-06-30 13:39:16', 'Masuk Telat 06:09:16'),
(62, '196108141990032001', 'Dra. Lilis Pujiawati', '2019-06-30', '2019-06-30 14:07:30', 'Masuk Telat 06:37:30'),
(63, '196711261990032001', 'Ely Sugiharti, S.Si., M.AP', '2019-06-30', '2019-06-30 14:07:42', 'Masuk Telat 06:37:42'),
(64, '196108141990032002', 'Ade Setyadi, S.I.Kom', '2019-06-30', '2019-06-30 14:08:42', 'Masuk Telat 06:38:42'),
(65, '196108141990032001', 'Dra. Lilis Pujiawati', '2019-06-30', '2019-06-30 14:08:52', 'Masuk Telat 06:38:52'),
(66, '196108141990032002', 'Ade Setyadi, S.I.Kom', '2019-06-30', '2019-06-30 14:11:35', 'Masuk Telat 06:41:35'),
(67, '196108141990032002', 'Ade Setyadi, S.I.Kom', '2019-06-30', '2019-06-30 14:21:24', 'Masuk Telat 06:51:24'),
(68, '196108141990032001', 'Dra. Lilis Pujiawati', '2019-06-30', '2019-06-30 14:21:35', 'Masuk Telat 06:51:35'),
(69, '196711261990032001', 'Ely Sugiharti, S.Si., M.AP', '2019-06-30', '2019-06-30 14:21:45', 'Masuk Telat 06:51:45'),
(70, '000000000000000000', 'admin', '2019-06-30', '2019-06-30 14:21:56', 'Masuk Telat 06:51:56'),
(71, '196108141990032002', 'Ade Setyadi, S.I.Kom', '2019-06-30', '2019-06-30 14:36:12', 'Masuk Telat 07:06:12'),
(72, '196108141990032010', 'arya', '2019-06-30', '2019-06-30 14:37:45', 'Masuk Telat 07:07:45'),
(73, '196108141990032001', 'Dra. Lilis Pujiawati', '2019-06-30', '2019-06-30 14:38:03', 'Masuk Telat 07:08:03'),
(74, '196108141990032002', 'Ade Setyadi, S.I.Kom', '2019-06-30', '2019-06-30 14:38:55', 'Masuk Telat 07:08:55'),
(75, '196108141990032010', 'arya', '2019-06-30', '2019-06-30 14:39:03', 'Masuk Telat 07:09:03'),
(76, '196108141990032001', 'Dra. Lilis Pujiawati', '2019-06-30', '2019-06-30 14:39:13', 'Masuk Telat 07:09:13'),
(77, '196711261990032001', 'Ely Sugiharti, S.Si., M.AP', '2019-06-30', '2019-06-30 14:39:38', 'Masuk Telat 07:09:38'),
(78, '196108141990032002', 'Ade Setyadi, S.I.Kom', '2019-06-30', '2019-06-30 14:42:21', 'Masuk Telat 07:12:21'),
(79, '196108141990032010', 'arya', '2019-06-30', '2019-06-30 14:42:28', 'Masuk Telat 07:12:28'),
(80, '196108141990032001', 'Dra. Lilis Pujiawati', '2019-06-30', '2019-06-30 14:42:39', 'Masuk Telat 07:12:39'),
(81, '196108141990032001', 'Dra. Lilis Pujiawati', '2019-06-30', '2019-06-30 14:44:56', 'Masuk Telat 07:14:56'),
(82, '196108141990032002', 'Ade Setyadi, S.I.Kom', '2019-06-30', '2019-06-30 14:45:13', 'Masuk Telat 07:15:13'),
(83, '196108141990032010', 'arya', '2019-06-30', '2019-06-30 14:45:18', 'Masuk Telat 07:15:18'),
(84, '196108141990032002', 'Ade Setyadi, S.I.Kom', '2019-06-30', '2019-06-30 14:46:07', 'pulang'),
(85, '196108141990032010', 'arya', '2019-06-30', '2019-06-30 14:46:14', 'pulang'),
(86, '196108141990032010', 'arya', '2019-06-30', '2019-06-30 18:17:09', 'Masuk Telat 10:47:09'),
(87, '196108141990032002', 'Ade Setyadi, S.I.Kom', '2019-06-30', '2019-06-30 18:17:18', 'Masuk Telat 10:47:18'),
(88, '196108141990032001', 'Dra. Lilis Pujiawati', '2019-06-30', '2019-06-30 18:17:29', 'Masuk Telat 10:47:29'),
(89, '196711261990032001', 'Ely Sugiharti, S.Si., M.AP', '2019-06-30', '2019-06-30 18:17:38', 'Masuk Telat 10:47:38'),
(90, '196711261990032001', 'Ely Sugiharti, S.Si., M.AP', '2019-06-30', '2019-06-30 18:20:44', 'Masuk Telat 10:50:44'),
(91, '196108141990032010', 'arya', '2019-06-30', '2019-06-30 19:22:00', 'Masuk Telat 11:52:00'),
(92, '196108141990032002', 'Ade Setyadi, S.I.Kom', '2019-06-30', '2019-06-30 19:50:08', 'Masuk Telat 12:20:08'),
(93, '196711261990032001', 'Ely Sugiharti, S.Si., M.AP', '2019-06-30', '2019-06-30 19:50:27', 'Masuk Telat 12:20:27'),
(94, '196711261990032001', 'Ely Sugiharti, S.Si., M.AP', '2019-06-30', '2019-06-30 19:58:08', 'Masuk Telat 12:28:08'),
(95, '196108141990032002', 'Ade Setyadi, S.I.Kom', '2019-06-30', '2019-06-30 20:23:49', 'Masuk Telat 12:53:49'),
(96, '196108141990032002', 'Ade Setyadi, S.I.Kom', '2019-06-30', '2019-06-30 20:24:01', 'pulang'),
(97, '196711261990032001', 'Ely Sugiharti, S.Si., M.AP', '2019-06-30', '2019-06-30 20:26:07', 'Masuk Telat 12:56:07'),
(98, '196108141990032002', 'Ade Setyadi, S.I.Kom', '2019-06-30', '2019-06-30 20:39:31', 'Masuk Telat 13:09:31'),
(99, '196108141990032010', 'arya', '2019-06-30', '2019-06-30 20:39:39', 'Masuk Telat 13:09:39'),
(100, '196711261990032001', 'Ely Sugiharti, S.Si., M.AP', '2019-06-30', '2019-06-30 20:39:46', 'Masuk Telat 13:09:46'),
(101, '196108141990032001', 'Dra. Lilis Pujiawati', '2019-06-30', '2019-06-30 20:40:06', 'Masuk Telat 13:10:06'),
(102, '196108141990032002', 'Ade Setyadi, S.I.Kom', '2019-06-30', '2019-06-30 21:04:04', 'Masuk Telat 13:34:04'),
(103, '196108141990032001', 'Dra. Lilis Pujiawati', '2019-06-30', '2019-06-30 21:04:14', 'Masuk Telat 13:34:14'),
(104, '196108141990032010', 'arya', '2019-06-30', '2019-06-30 21:04:28', 'Masuk Telat 13:34:28'),
(105, '196108141990032010', 'arya', '2019-06-30', '2019-06-30 21:09:31', 'Masuk Telat 13:39:31'),
(106, '196108141990032001', 'Dra. Lilis Pujiawati', '2019-06-30', '2019-06-30 21:09:38', 'Masuk Telat 13:39:38'),
(107, '196108141990032002', 'Ade Setyadi, S.I.Kom', '2019-06-30', '2019-06-30 21:09:57', 'Masuk Telat 13:39:57'),
(108, '196711261990032001', 'Ely Sugiharti, S.Si., M.AP', '2019-06-30', '2019-06-30 21:18:15', 'Masuk Telat 13:48:15'),
(109, '196108141990032001', 'Dra. Lilis Pujiawati', '2019-07-01', '2019-07-01 15:48:04', 'Masuk Telat 08:18:04'),
(110, '196108141990032001', 'Dra. Lilis Pujiawati', '2019-07-01', '2019-07-01 18:11:43', 'Masuk'),
(111, '196711261990032001', 'Ely Sugiharti, S.Si., M.AP', '2019-07-01', '2019-07-01 18:17:15', 'Masuk Telat 01:12:45'),
(112, '196108141990032002', 'Ade Setyadi, S.I.Kom', '2019-07-01', '2019-07-01 18:52:28', 'Masuk Telat 00:37:32'),
(113, '196108141990032010', 'arya', '2019-07-01', '2019-07-01 18:53:18', 'Masuk Telat 00:36:42'),
(114, '196108141990032002', 'Ade Setyadi, S.I.Kom', '2019-07-03', '2019-07-03 16:52:04', 'Masuk Telat 02:37:56'),
(115, '196108141990032010', 'arya', '2019-07-03', '2019-07-03 16:53:18', 'Masuk Telat 02:36:42'),
(116, '196108141990032002', 'Ade Setyadi, S.I.Kom', '2019-07-03', '2019-07-03 17:03:03', 'Masuk'),
(117, '196108141990032001', 'Dra. Lilis Pujiawati', '2019-07-03', '2019-07-03 17:03:13', 'Masuk'),
(118, '196711261990032001', 'Ely Sugiharti, S.Si., M.AP', '2019-07-03', '2019-07-03 17:03:22', 'Masuk'),
(119, '196108141990032010', 'arya', '2019-07-03', '2019-07-03 17:03:31', 'Masuk'),
(120, '196108141990032002', 'Ade Setyadi, S.I.Kom', '2019-07-03', '2019-07-03 17:08:01', 'Masuk'),
(121, '196108141990032001', 'Dra. Lilis Pujiawati', '2019-07-03', '2019-07-03 17:08:09', 'Masuk'),
(122, '196108141990032002', 'Ade Setyadi, S.I.Kom', '2019-07-03', '2019-07-03 17:15:07', 'Masuk'),
(123, '196108141990032001', 'Dra. Lilis Pujiawati', '2019-07-03', '2019-07-03 17:15:16', 'Masuk'),
(124, '196108141990032002', 'Ade Setyadi, S.I.Kom', '2019-07-03', '2019-07-03 17:18:21', 'Masuk'),
(125, '196108141990032001', 'Dra. Lilis Pujiawati', '2019-07-03', '2019-07-03 17:18:28', 'Masuk'),
(126, '196108141990032010', 'arya', '2019-07-04', '2019-07-04 19:04:03', 'Masuk Telat 01:34:03'),
(127, '196108141990032001', 'Dra. Lilis Pujiawati', '2019-07-04', '2019-07-04 19:12:06', 'Masuk Telat 01:42:06'),
(128, '196108141990032002', 'Ade Setyadi, S.I.Kom', '2019-07-04', '2019-07-04 19:13:09', 'Masuk Telat 01:43:09'),
(129, '196711261990032001', 'Ely Sugiharti, S.Si., M.AP', '2019-07-04', '2019-07-04 19:13:54', 'Masuk Telat 01:43:54'),
(130, '196108141990032001', 'Dra. Lilis Pujiawati', '2019-07-04', '2019-07-04 19:17:27', 'Masuk Telat 01:47:27'),
(131, '196108141990032001', 'Dra. Lilis Pujiawati', '2019-07-04', '2019-07-04 19:18:43', 'Masuk Telat 01:48:43'),
(132, '196108141990032010', 'arya', '2019-07-04', '2019-07-04 19:34:54', 'Masuk Telat 02:04:54'),
(133, '196108141990032010', 'arya', '2019-07-04', '2019-07-04 19:37:21', 'Masuk Telat 02:07:21'),
(134, '196108141990032010', 'arya', '2019-07-04', '2019-07-04 19:48:10', 'Masuk Telat 02:18:10'),
(135, '196108141990032002', 'Ade Setyadi, S.I.Kom', '2019-07-04', '2019-07-04 20:18:42', 'Masuk Telat 02:48:42'),
(136, '196711261990032029', 'kumbang', '2019-07-05', '2019-07-05 07:34:39', 'Masuk Telat 00:04:39'),
(137, '196108141990032001', 'Dra. Lilis Pujiawati', '2019-07-05', '2019-07-05 07:38:49', 'Masuk Telat 00:08:49'),
(138, '196108141990032002', 'Ade Setyadi, S.I.Kom', '2019-07-07', '2019-07-07 08:31:12', 'Masuk Telat 01:01:12'),
(139, '196108141990032002', 'Ade Setyadi, S.I.Kom', '2019-07-07', '2019-07-07 13:32:17', 'Masuk Telat 01:02:17'),
(140, '196108141990032001', 'Dra. Lilis Pujiawati', '2019-07-20', '2019-07-20 17:12:12', 'Masuk Telat 00:02:12'),
(141, '196108141990032001', 'Dra. Lilis Pujiawati', '2019-07-22', '2019-07-22 17:42:22', 'Masuk Telat 00:32:22'),
(142, '196108141990032001', 'Dra. Lilis Pujiawati', '2019-07-22', '2019-07-22 17:45:43', 'Masuk Telat 00:35:43'),
(143, '196108141990032001', 'Dra. Lilis Pujiawati', '2019-07-20', '2019-07-20 17:46:15', 'Masuk Telat 00:36:15'),
(144, '196108141990032001', 'Dra. Lilis Pujiawati', '2019-07-22', '2019-07-22 17:48:52', 'Masuk Telat 00:38:52'),
(145, '196108141990032010', 'arya', '2019-07-20', '2019-07-20 17:51:57', 'Masuk Telat 00:41:57'),
(146, '000000000000000000', 'admin', '2019-07-21', '2019-07-21 02:15:14', 'Masuk');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pegawai`
--

CREATE TABLE `pegawai` (
  `id` int(20) NOT NULL,
  `nip` varchar(20) DEFAULT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `kota` varchar(50) DEFAULT NULL,
  `alamat` tinytext,
  `id_divisi` int(11) DEFAULT NULL,
  `id_jabatan` int(11) DEFAULT NULL,
  `aktif_pegawai` int(11) DEFAULT '1',
  `password_pegawai` varchar(50) DEFAULT NULL,
  `ip_address` varchar(50) DEFAULT NULL,
  `last_activity` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `qr_code` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pegawai`
--

INSERT INTO `pegawai` (`id`, `nip`, `nama`, `kota`, `alamat`, `id_divisi`, `id_jabatan`, `aktif_pegawai`, `password_pegawai`, `ip_address`, `last_activity`, `qr_code`) VALUES
(6, '000000000000000000', 'admin', 'Bandung', 'Sarijadi', 17, 11, 1, '25d55ad283aa400af464c76d713c07ad', '192.168.1.10', '2019-03-31 08:21:57', '111111111111111000.png'),
(8, '196108141990032002', 'Ade Setyadi, S.I.Kom', 'Cimahi', 'Jl. Rancabentang No. 418 RT. 05 RW. 26 Kel. Cibeureum, Kec. Cimahi Selatan, Cimahi', 18, 13, 1, '196108141990032002', '192.168.1.102', '2019-07-23 13:03:46', '196108141990032002.png'),
(10, '196108141990032001', 'Dra. Lilis Pujiawati', 'Bandung', 'Komplek Cibolerang Blok L No. 5, Jl.Satria Raya RT. 07 RW. 07 Kel. Margahayu Utara, Kec. Babakan Ciparay, Bandung', 1, 1, 1, '196108141990032001', '', '2019-07-20 19:06:52', '196108141990032001.png'),
(11, '196711261990032001', 'Ely Sugiharti, S.Si., M.AP', 'Bandung', 'Bumi Panyileukan Blok G.15 No. 20 RT. 01 RW. 07 Kel. Cipadung, Kec. Panyileukan, Bandung', 18, 12, 1, '196711261990032001', '', '2019-04-21 11:59:37', '195802061996031000.png'),
(12, '196108141990032010', 'arya', 'Tarutung', 'sdf', 20, 16, 1, '196108141990032010', NULL, '2019-07-20 19:30:54', '196108141990032010.png'),
(13, '196711261990032029', 'kumbang', 'Tarutung', 'sarijadi', 15, 7, 1, '196711261990032029', NULL, '2019-07-05 00:32:37', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengaturan`
--

CREATE TABLE `pengaturan` (
  `id` int(11) NOT NULL,
  `nama_perusahaan` varchar(50) DEFAULT NULL,
  `alamat` varchar(50) DEFAULT NULL,
  `mulai_absensi` time DEFAULT NULL,
  `mulai_masuk` time DEFAULT NULL,
  `mulai_pulang` time DEFAULT NULL,
  `timeout_qr_code` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pengaturan`
--

INSERT INTO `pengaturan` (`id`, `nama_perusahaan`, `alamat`, `mulai_absensi`, `mulai_masuk`, `mulai_pulang`, `timeout_qr_code`) VALUES
(1, 'Badan Pusat Statistik Kota Bandung', 'Jl. Jendral Gatot Subroto No. 93, Bandung', '01:00:00', '02:30:00', '22:00:00', '23:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `username` varchar(20) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `id_pegawai` int(11) DEFAULT NULL,
  `id_level` int(11) DEFAULT NULL,
  `aktif` int(11) DEFAULT NULL,
  `create_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` timestamp NULL DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id_user`, `username`, `password`, `id_pegawai`, `id_level`, `aktif`, `create_date`, `update_date`, `last_login`) VALUES
(7, 'admin', '72a40b5e4f707b270fa0c18e8b483a94', 6, 3, 1, '2019-03-24 08:14:34', NULL, NULL),
(8, 'ade', 'a562cfa07c2b1213b3a5c99b756fc206', 8, 2, 1, '2019-06-27 02:02:54', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `absensi`
--
ALTER TABLE `absensi`
  ADD PRIMARY KEY (`id_absensi`);

--
-- Indexes for table `coba`
--
ALTER TABLE `coba`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `cuti`
--
ALTER TABLE `cuti`
  ADD PRIMARY KEY (`id_cuti`);

--
-- Indexes for table `dinas`
--
ALTER TABLE `dinas`
  ADD PRIMARY KEY (`id_dinas`);

--
-- Indexes for table `divisi`
--
ALTER TABLE `divisi`
  ADD PRIMARY KEY (`id_divisi`);

--
-- Indexes for table `harilibur`
--
ALTER TABLE `harilibur`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jabatan`
--
ALTER TABLE `jabatan`
  ADD PRIMARY KEY (`id_jabatan`);

--
-- Indexes for table `level`
--
ALTER TABLE `level`
  ADD PRIMARY KEY (`id_level`);

--
-- Indexes for table `log_activity`
--
ALTER TABLE `log_activity`
  ADD PRIMARY KEY (`id_log`);

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pengaturan`
--
ALTER TABLE `pengaturan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `absensi`
--
ALTER TABLE `absensi`
  MODIFY `id_absensi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=135;
--
-- AUTO_INCREMENT for table `divisi`
--
ALTER TABLE `divisi`
  MODIFY `id_divisi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `harilibur`
--
ALTER TABLE `harilibur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `jabatan`
--
ALTER TABLE `jabatan`
  MODIFY `id_jabatan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `level`
--
ALTER TABLE `level`
  MODIFY `id_level` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `log_activity`
--
ALTER TABLE `log_activity`
  MODIFY `id_log` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=147;
--
-- AUTO_INCREMENT for table `pegawai`
--
ALTER TABLE `pegawai`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `pengaturan`
--
ALTER TABLE `pengaturan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
